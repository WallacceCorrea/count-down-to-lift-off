const countdownDisplay = document.getElementById("countdown-display");
const rocket = document.getElementById("rocket-group");
const fire = document.getElementById("rocket-fire");
const puff = document.getElementById("rocket-puff");
const restartBtn = document.getElementById("restart-btn")
let timeRemaining = 5;
let message = "Ignition!"; //sets the initial message after countdown

// Task:
// 1. Write the JavaScript to count down from 60 to 0 and display the remaining time


const countDown = () => {
    if (timeRemaining > 0) {
        timeRemaining--;
        displayMessages();
    }
};

// handle messages to the user
const displayMessages = () => {
    if (timeRemaining >= 1) {
        getMessages(timeRemaining);
    } else if (timeRemaining === 0) {
        getMessages(message); //default message
        isAGo() //check if the mission will be accomplished
        setTimeout(function () {
            getMessages(message); // wait 1 sec to display the radomized results
        }, 1000)
        endGame()
    }
};
//DRY
const getMessages = (input) => {
    countdownDisplay.textContent = input;
};

// 1) Add fire under the rocket && 2. When the countdown reaches 0, launch the rocket with an animation
const launchRocket = (option) => {
    if (option === 1) {
        rocket.classList.add("rocket-fly");
        fire.classList.add("ignites");
        fire.style.opacity = 1;
    } else if (option === 2) {
        puff.classList.add("ignites");
    } else if (option === 3) {
        rocket.classList.add("rocket-die");
        fire.classList.add("ignites-fail");
    }
};

// 2) Randomly don’t launch the rocket when the timer reaches 0.

const isAGo = () => {
    let lackOfCoffee = Math.random();
    if (lackOfCoffee < 0.4) {
        launchRocket(1);
        message = "Eagle flies!";
    } else if (lackOfCoffee < 0.8) {
        launchRocket(2);
        message = "Ignition failed! JS?!";
    } else {
        //absolute lack of cafeine
        launchRocket(3);
        message = "Ooops...";
    }
};

const renderGame = () => {
    restartBtn.style.opacity = 0;
    setInterval(countDown, 1000)
    getMessages(timeRemaining)
};

const restartGame = () => {
    window.location.reload()
}

restartBtn.addEventListener("click", restartGame);

const endGame = () => {
    setTimeout(function () {
        countdownDisplay.textContent = "" // erase message's content
        restartBtn.style.opacity = 1;
    }, 4000);
}

renderGame();
